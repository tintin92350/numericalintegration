/*! \mainpage Numerical Integration Program
 * 
 * \section intro_sec Introduction
 * 
 *
 * Numerical Integration Program is a student free open-source program made by student
 * to compute an approximation value of an integral of any evaluable maths expression
 * between any interval.
 * 
 * The program can check the approximation error of any integrable function (primitive check)
 * 
 * \section work_math_sec How works the program (maths definition)
 * 
 * <ul>
 * <li>Let \f$(a, b)\in\mathbb{R}^2\f$ with \f$a < b\f$.</li>
 * <li>Let f a real continuous function on \f$[a, b]\f$ and \f$n \in\mathbb{N}^*\f$</li>
 * <li>We have \f$a_0 < a_1 < ... < a_n\f$ with \f$a = a(0) = a_0\f$ and \f$b = a(n) = a_n\f$ and \f$\forall k \in [0, n - 1]\f$ we have :<br/>
 * \f[a_{k+1} - a_k = h = \frac{b - a}{n}\f]</li>
 * 
 * <li>Finally we can say that :
 * \f[\int_{a}^b f(t)dt = \sum\limits_{k=0}^{k=n-1} \int_{a_k}^{a_{k+1}} f(t)dt\f]
 *</li>
 *</ul>
 *
 * The program will compute each value thanks to 5 algorithms based on 5 maths methods :
 * <ul>
 *  <li>Left Rectangle Rule <a href="https://en.wikipedia.org/wiki/Numerical_integration#Quadrature_rules_based_on_interpolating_functions" target="_blank" >Wikipedia (en)</a></li>
 *  <li>Right Rectangle Rule <a href="https://en.wikipedia.org/wiki/Numerical_integration#Quadrature_rules_based_on_interpolating_functions" target="_blank" >Wikipedia (en)</a></li>
 *  <li>Median Rectangle Rule <a href="https://en.wikipedia.org/wiki/Numerical_integration#Quadrature_rules_based_on_interpolating_functions" target="_blank" >Wikipedia (en)</a></li>
 *  <li>Trapezoidal Rule <a href="https://en.wikipedia.org/wiki/Trapezoidal_rule" target="_blank" > Wikipedia (en)</a></li>
 *  <li>Simpson Rule <a href="https://en.wikipedia.org/wiki/Simpson%27s_rule" target="_blank" >Wikipedia (en)</a></li>
 * </ul>
 * You can see each methods in this file : source/methods.h (each formula is provided)
 * 
 * \section install_sec Installation
 *
 * \subsection step1 Step 1: Download the source code from the website
 * Go to this webpage <a href="#" >here</a> then download the source code (tar.gz)
 * \subsection step2 Step 2: Extract the file
 * Extract the downloaded archive into the directory you want with no sudo permission !
 * \subsection step3 Step 3: Execute the install bash script
 * Execute the install bash script provided to generate directory into user bin and symbolic link to use the program
 * more friendly. The installer will also check if apache and php are installed then install NIWI (see the next section for further information).
 *
 * \section install_niwi_sec Installation of web interface
 * 
 * Numerical Integration provide a web interface that use
 * apache server and php to work well.
 * 
 * So first install this two package:
 * \code {.sh}
 * apt install apache2
 * apt install php
 * \endcode
 *
 * Then install NIWI by the provided install script:
 * \code {.sh}
 * bash install-web-interface.sh
 * \endcode
 * 
 * <em><b>This script can only work in basic and default debian apache2 distribution
 * since it install the NIWI under the /var/www/html directory</b></em>
 * 
 * \section exec_sec Execution
 * 
 * To execute the program go to the bin directory then type (for example) : <br/>
 * \code{.sh}
 * ./NumericalIntegration x^2 0 1
 * \endcode
 * 
 * The above example show the 5 values computed for \f$f(x) = x^2\f$ with \f$x\in[0, 1]\f$ <br/><br/>
 * 
 * \section exmpl_sec Use
 * 
 * \subsection normal Default argument (simple)
 * The simple way to use the program is with this generic code :
 * \code{.sh}
 * ./NumericalIntegration f a b
 * \endcode
 * The program take in argument the function \f$f(x)\f$ and the interval \f$[a, b]\f$
 * 
 * \subsection step Step argument
 * You can use the argument -n to precise the step number. \f$ n\in[1, +infty[\f$
 * \code{.sh}
 * ./NumericalIntegration f a b -n 1000
 * \endcode
 * The program take in argument the function \f$f(x)\f$ and the interval \f$[a, b]\f$ and then the n argument that says that we use a step of 1000
 * 
 * \subsection prim Antiderivate (primitive) argument
 * You can use the argument -F to provide the primitive of the function set. Then whatever the output could be it will
 * display the approximation error of each computed values.
 * \code{.sh}
 * ./NumericalIntegration f a b -F 1/3*x^3
 * \endcode
 * The program take in argument the function \f$f(x)\f$ and the interval \f$[a, b]\f$ and then one of the primitive of \f$x^2\f$
 * 
 * \subsection html_out HTML Document output
 * You can use the argument -g tell to the program to only generate an HTML document
 * instead of the terminal output
 * \code{.sh}
 * ./NumericalIntegration f a b -g
 * \endcode
 * The program take in argument the function \f$f(x)\f$ and the interval \f$[a, b]\f$ and then g argument.
 * The program will generate an HTML document that could be open by a browser.
 * 
 * \subsection webfile_out NIWI output
 * You can use the argument -wf to generate a standard output formated
 * to be used by NIWI program.
 * \code{.sh}
 * ./NumericalIntegration f a b -wf
 * \endcode
 * The output will be the function and the 5 computed values without
 * name. With primitive set, it add the expected value and the fives 
 * approximation error and percentage
 *
 * \subsection webinterface_sect NIWI opening
 * In order to open the web interface with the CLI you must use : 
 * \code{.sh}
 * ./NumericalIntegration -w
 * \endcode
 * 
 * If you want to have a NIWI output use the program as you wish and use the -w argument:
 * \code{.sh}
 * ./NumericalIntegration f a b -w
 * \endcode
 */

/**
 * @file main.c
 * @author RODIC Quentin (rodic.quentin.corporation@gmail.com)
 * @brief Program that compute the integral by 5 different ways
 * @version 0.1
 * @date 2019-09-13
 * 
 * @copyright Copyright (c) 2019
 * 
 */

// C STANDARD
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

// Constants
#include "constants.h"

// Methods
#include "methods.h"

/**
 * @brief Define a C string
 */
typedef char * string;

/**
 * @brief Compute the approximation error and percentage then
 * display it on terminal output.
 * 
 * @param rvalue Computed value
 * @param evalue Expected value (by primitive way)
 * @param checkbyprim 1 if the primitive was entered
 */
void display_expectation_difference(REAL rvalue, REAL evalue, int checkbyprim)
{
    if(checkbyprim == 1)
    {
        REAL approximation_error = fabs(rvalue - evalue);
        REAL approximation_error_p = 100.0 * (approximation_error / rvalue);

        const char * color = "[0m";

        if(approximation_error_p >= 75.0)
            color = "\033[0;31m";
        else if(approximation_error_p >= 25.0)
            color = "\033[0;33m";
        else
            color = "\033[0;32m";

        printf("The expected value (real) : \t%0.10f\nThe approximated value : %s\t%0.10f\033[0m\n", evalue, color, rvalue);
        printf("An absolute error of %0.10f (namely %0.10f%%)\n\n", approximation_error, approximation_error_p);
    }
    else
    {
        printf("The approximated value computed is \t%0.10f\n\n", rvalue);
    }
}

/**
 * @brief Copy the current date of the execution
 * into the buffer passed in argument
 * 
 * @param dst Destination buffer
 */
void cpydttostr(char * dst)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(dst, "%d/%d/%d", tm.tm_mon+1, tm.tm_mday, tm.tm_year+1900);
}

/**
 * @brief Copy the current date of the execution
 * into the buffer passed in argument in a formated format
 * 
 * @param dst Destination buffer
 * @param format Format that should contains only 6 integer
 */
void cpydttostr_f(char * dst, const char * format)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(dst, format, tm.tm_mon+1, tm.tm_mday, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
}

/**
 * @brief Generate an html table content for a particular method
 * then store it into a destination buffer
 * 
 * @param dst Destination string pointer
 * @param method_name Name of the method
 * @param computed Computed value of integral
 * @param expected Expected value of integral
 * @param normal If normal is equals to 1 we just set normal style class for HTML5 argument
 */
void generate_method_html_content(char ** dst, const char * method_name, REAL computed, REAL expected, int normal)
{
    // Compute the error of approximation
    // and its percentage
    REAL approximation_error = fabs(computed - expected);
    REAL approximation_error_p = 100.0 * (approximation_error / computed);

    // Assign a good, middle or bad class to html so as the error is big
    char * err_state = (normal == 1) ? "normal" : approximation_error_p >= 75.0 ? "bad" : approximation_error_p >= 25.0 ? "middle" : "good";

    // Compute the space we need to allocate the buffer to store the whole line
    size_t byte_needed = snprintf(NULL, 0, HTML_METHOD_CONTENT, method_name, expected, err_state, computed, err_state, approximation_error, approximation_error_p);
    
    // We allocate the destination with the previous computed space more 1
    (*dst) = (char*)malloc(byte_needed+1);

    // Copy the whole line to the buffer
    sprintf((*dst), HTML_METHOD_CONTENT, method_name, expected, err_state, computed, err_state, approximation_error, approximation_error_p);
}

/**
 * @brief Generate the whole HTML Document that will store all the 5-ways methods values
 * computed and variables used in the program
 * 
 * @param computed Computed value of the integral
 * @param expected Expected value of the integral (by primitive)
 * @param f_expr Expression of the function f(x)
 * @param F_expr Expression of the primitive of the function F(x)
 * @param a Interval begining
 * @param b Interval ending
 * @param n Subdivision
 */
void generate_html_document(REAL * computed, REAL expected, const char * f_expr, const char *F_expr, REAL a, REAL b, NATURAL n)
{
    // Final string that will be copied in the output file
    char * html_doc;

    // Date string that match to when the program is executed
    // The maximum value of the date is in space : 00/00/0000 that mean 10 bytes
    char date_str[10];

    // Table content stored in 5 temps arrays
    // that match to each methods used in the program and their output
    char * methods_html_tab_tmps[5];

    // Whole table content where will be coped each temporary buffer
    char * methods_html_table;

    // Copy the date of today in the buffer
    cpydttostr(date_str);

    // Generate for each method its HTML table content
    for(size_t i = 0; i < 5; i++) {
        generate_method_html_content(&methods_html_tab_tmps[i], METHODS_NAME[i], computed[i], expected, strcmp(F_expr, "") == 0 ? 1 : 0);
    }

    // Compute the final space in byte of the whole table content
    size_t space_byte_table_content = strlen(methods_html_tab_tmps[0]) + strlen(methods_html_tab_tmps[1]) + strlen(methods_html_tab_tmps[2]) + strlen(methods_html_tab_tmps[3]) + strlen(methods_html_tab_tmps[4]) + 1;

    // Allocate the methods table array and initialise each byte
    // to a zero value as we avoid strcat error
    methods_html_table = (char*)malloc(sizeof(char) * space_byte_table_content);
    memset(methods_html_table, 0, sizeof(char) * space_byte_table_content);

    // Concatenate all string that we created before
    // by generating html content to a unique string
    for(size_t i = 0; i < 5; i++)
    {
        strcat(methods_html_table, methods_html_tab_tmps[i]);
        free(methods_html_tab_tmps[i]);
    }

    // Compute the space we need to store the whole HTML document into the buffer
    size_t whole_space_byte = snprintf(NULL, 0, HTML5_DOC_SOURCE, date_str, f_expr, F_expr, a, b, n, methods_html_table) + 1;

    // Allocate and initialise to zero the whole html document buffer
    html_doc = (char*)malloc(sizeof(char) * whole_space_byte);
    memset(html_doc, 0, sizeof(char) * whole_space_byte);

    // Print the whole buffer to the html string
    sprintf(html_doc, HTML5_DOC_SOURCE, date_str, f_expr, F_expr, a, b, n, methods_html_table);

    // Open the output file
    char filename[27 + 19];
    char file_date[19];

    memset(filename, 0, 27 + 19);
    memset(file_date, 0, 19);

    strcpy(filename, "numerical-integration-");
    cpydttostr_f(file_date, "%d_%d_%d_%d_%d_%d");
    strcat(filename, file_date);
    strcat(filename, ".html");
    printf("%s\n", filename);
    FILE * f = fopen(filename, "w");

    if(f)
    {
        fputs(html_doc, f);
        fclose(f);
    }

    // Free allocated memories
    free(methods_html_table);
    free(html_doc);
}

/**
 * @brief Interpret a string as a value by tinyexpr
 * 
 * @param buf Input buffer
 * @return Interpreted argument number
 */
REAL interpret_string(const char * buf)
{
    return te_interp(buf, 0);
}

/**
 * @brief Main entry point of the program
 * 
 * @param argc Number of arguments
 * @param argv Arguments list
 * @return return code 
 */
int main(int argc, char ** argv)
{
    // Check if only the web interface is wanted
    if(argc == 2)
    {
        if(strcmp(argv[1], "-w") == 0) // The user wants to open the web interface
        {
            system("firefox 127.0.0.1/niwi/");
            return 0;
        }
    }

    // We left the program if the minimum of arguments
    // are not sets in the terminal
    if(argc < 4) {
        printf("Some parameters missed : \n");
        printf("[function expression] [a] [b] (n) (primitive expression)\n");
        return EXIT_FAILURE;
    }

    int output_way = TERMINAL_OUTPUT;   // By default we output on terminal
    int check_by_primitive = 0;         // By default we do not check any results by its primitive
    REAL a = 0.0;                       // By default we have an interval that begin at 0
    REAL b = 1.0;                       // By default we have en interval that end at 1
    NATURAL n = 10;                     // By default we have a step of 10 (optional)
    string function_expr = "";          // By default we have no function (must be provided in argument)
    string primitive_expr = "";         // By default we provide no primitive for the function
    int open_web_interface = 0;         // The program open the web interface
    
    // Expression structure to apply f(x)
    EXPRESSION f_expr, F_expr = { NULL, {}, 0.0, 0};

    // Check all needed argument type and validity
    // then store them into variable is verfied
    function_expr = argv[1];

    a = interpret_string(argv[2]);
    b = interpret_string(argv[3]);

    // We swap a with b if b is greater than a
    swap_greater(&a, &b);

    // We scan another argument provided with the program
    // if we have an argument count more than 4
    if(argc > 4)
    {
        for(int i = 0; i < argc; i++)
        {
            if(strcmp(argv[i], "-n") == 0) // The user provided a n argument (step)
            {
                if(i + 1 < argc) // We check if the next argument has been sets
                {
                    if(isnumeric(argv[i+1], strlen(argv[i+1]))) // If the provided next argument is a number
                    {
                        n = atoi(argv[i+1]);
                        if(n <= 1)
                            n = 2;

                        i++;
                    }
                }
            }
            else if(strcmp(argv[i], "-F") == 0) // The user provided a F argument (Primitive)
            {
                if(i + 1 < argc) // We check if the next argument has been sets
                {
                    primitive_expr = argv[i+1];
                    check_by_primitive = 1;
                    i++;
                }
            }
            else if(strcmp(argv[i], "-g") == 0) // The user provided a F argument (Primitive)
            {
                output_way = HTML_DOCUMENT_OUTPUT;
            }
            else if(strcmp(argv[i], "-wf") == 0) // The user provided a F argument (Primitive)
            {
                output_way = WEB_FILE_OUTPUT;
            }
            else if(strcmp(argv[i], "-w") == 0) // The user wants to open the web interface
            {
                open_web_interface = 1;
            }
        }
    }

    if(open_web_interface)
    {
        char command[500];
        
        if(!check_by_primitive)
            sprintf(command, "firefox '127.0.0.1/niwi/index.php?a=%0.10f&b=%0.10f&f='%s'&n=%d'", a, b, function_expr, n);
        else
            sprintf(command, "firefox '127.0.0.1/niwi/index.php?a=%0.10f&b=%0.10f&f='%s'&F='%s'&n=%d'", a, b, function_expr, primitive_expr, n);
        printf("command : %s\n", command);
        system(command);
        return 0;
    }

    // Compute the expressions to be used later
    // and the primitive expression if given
    compute_expression(&f_expr, function_expr);

    if(check_by_primitive) 
        compute_expression(&F_expr, primitive_expr);

    // Array of calculated values
    REAL values[5] = {
        integral_by_left_rectangle_rule(&f_expr, a, b, n),
        integral_by_right_rectangle_rule(&f_expr, a, b, n),
        integral_by_median_rectangle_rule(&f_expr, a, b, n),
        integral_by_trapezoidal_rule(&f_expr, a, b, n),
        integral_by_simpson_rule(&f_expr, a, b, n)
    };

    REAL expected = check_by_primitive ? f(&F_expr, b) - f(&F_expr, a) : NAN;
        
    // Print on terminal output
    if(output_way == TERMINAL_OUTPUT)
    {
        #if defined(__linux__)
        system("clear");
        #elif defined(WIN32)
        system("clr");
        #endif

        printf("********************************************************************\n");
        printf("*                                                                  *\n");
        printf("* Numerical Integration Program (terminal)                         *\n");
        printf("* Created by Quentin RODIC                                         *\n");
        printf("* For L'IUT de Vélizy                                              *\n");
        printf("* Subject : Probabilities and Statistics                           *\n");
        printf("*                                                                  *\n");
        printf("********************************************************************\n\n");

        printf("\033[0;36m## Variable used in the program ###################################\n\033[0m");
        printf("a = %0.10f\tb = %0.10f\tn = %i\n", a, b, n);
        printf("f(x) = %s\nF(x) = %s\n\n", function_expr, check_by_primitive ? primitive_expr : "no specified");
        
        //
        // Calcul using the left rectangle rule
        //
        printf("\033[0;36m## Left Rectangle Rule ############################################\n\033[0m");
        display_expectation_difference(values[0], expected, check_by_primitive);

        //
        // Calcul using the right rectangle rule
        //
        printf("\033[0;36m## Right Rectangle Rule ###########################################\033[0m\n");
        display_expectation_difference(values[1], expected, check_by_primitive);

        //
        // Calcul using the median rectangle rule
        //
        printf("\033[0;36m## Median Rectangle Rule ##########################################\033[0m\n");
        display_expectation_difference(values[2], expected, check_by_primitive);    

        //
        // Calcul using the trapezoidal rule
        //
        printf("\033[0;36m## Trapezoidal Rule ###############################################\033[0m\n");
        display_expectation_difference(values[3], expected, check_by_primitive);
        
        //
        // Calcul using the simpson rectangle rule
        //
        printf("\033[0;36m## Simpson Rule ###################################################\n\033[0m");
        display_expectation_difference(values[4], expected, check_by_primitive);
    }
    // Print on HTML document
    else if(output_way == HTML_DOCUMENT_OUTPUT)
    {
        generate_html_document(values, expected, function_expr, primitive_expr, a, b, n );
        printf("HTML document generated !\n");
    }
    else if(output_way == WEB_FILE_OUTPUT)
    {
        printf("%s\n", function_expr);
        printf("%0.10f\n", values[0]);
        printf("%0.10f\n", values[1]);
        printf("%0.10f\n", values[2]);
        printf("%0.10f\n", values[3]);
        printf("%0.10f\n", values[4]);
        
        if(check_by_primitive)
        {
            printf("%0.10f\n", expected);
            for (int i = 0; i < 5; i++)
            {
                REAL approximation_error = fabs(values[i] - expected);
                REAL approximation_error_p = 100.0 * (approximation_error / values[i]);

                printf("%0.10f\n", approximation_error);
                printf("%0.10f\n", approximation_error_p);
            }
        }
    }

    return EXIT_SUCCESS;
}
