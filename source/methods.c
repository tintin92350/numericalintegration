#include "methods.h"

REAL integral_by_left_rectangle_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n)
{
    REAL sum = 0.0;
    REAL h = (b - a) / (REAL)n;
    REAL k = 0.0;

    // Sum from 0 to n-1 included
    while(k <= n - 1)
    {
        sum += f(expr, a + (REAL)k * h);
        k++;
    }

    return h * sum;
}

REAL integral_by_right_rectangle_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n)
{    
    REAL sum = 0.0;
    REAL h = (b - a) / (REAL)n; 
    NATURAL k = 1.0;

    // Sum from 0 to n included
    while(k <= n)
    {
        sum += f(expr, a + (REAL)k * h);
        k++;
    }

    return h * sum;
}

REAL integral_by_median_rectangle_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n)
{
    REAL sum = 0.0;
    REAL h = ((REAL)b - (REAL)a) / (REAL)n;
    NATURAL k = 0;
    
    while(k <= n - 1)
    {
        sum += f(expr, ((a + (REAL)k * h) + (a + (REAL)(k+1.0) * h)) / 2.0);
        k++;
    }

    return h * sum;
}

REAL integral_by_trapezoidal_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n)
{    
    REAL sum = 0.0;
    REAL h = (b - a) / (REAL)n;
    REAL fa = f(expr, a);
    REAL fb = f(expr, b);
    NATURAL k = 1;

    while(k <= n - 1)
    {
        sum += f(expr, a + (REAL)k * h);
        k++;
    }

    return (b-a)/(2.0 * (REAL)n) * (fa + fb + 2.0 * sum);
}

REAL integral_by_simpson_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n)
{    
    REAL suma = 0.0, sumb = 0.0;
    REAL h = (b - a) / (REAL)(6.0*n);
    REAL fa = f(expr, a);
    REAL fb = f(expr, b);
    REAL k = 0.0;

    while(k <= n - 1)
    {
        sumb += f(expr, a + ((2.0 * k + 1) * (b - a)) / (2.0 * n ));    
        if(k > 0)
            suma += f(expr, a + (k * (b - a)) / n);
        k++;
    }

    return h * (fa + fb + 2.0 * suma + 4.0 * sumb);
}