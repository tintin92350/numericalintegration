/**
 * @file maths.h
 * @author RODIC Quentin (rodic.quentin.corporation@gmail.com)
 * @brief Define all types for math algorithms and utils functions
 * @version 0.1
 * @date 2019-09-14
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef MY_MATHS_HEADER
#define MY_MATHS_HEADER

// C STANDARD
#include <ctype.h>

// TinyExpr library header
#include "tinyexpr.h"

/**
 * @brief Define a maths real number (\f$\mathbb{R}\f$)
 */
typedef double          REAL;

/**
 * @brief Define a maths natural number (\f$\mathbb{N}\f$)
 */
typedef unsigned int    NATURAL;

/**
 * @brief Define a maths integer number (\f$\mathbb{Z}\f$)
 */
typedef int             INTEGER;

/**
 * Structure that hold an expression with its attached
 * variable (x) and error.
 * That structure is formed to be used with TinyExpr library 
 */
typedef struct ExpressionHolder {
    te_expr * computable;   /*!< The math expression */
    te_variable vars[1];    /*!< Variable array for tinyexpr */
    REAL x;                 /*!< x maths variable for the function */
    int err;                /*!< Error code */
} EXPRESSION;

/**
 * @brief Returns true if the string is a number
 * 
 * @param str String to check
 * @param n String length
 * @return int 
 */
int isnumeric(const char * str, const int n);

/**
 * @brief Swap the values if right operand is greater
 * than the left operand
 * 
 * @param ops1 Left operand
 * @param ops2 Right operand
 */
void swap_greater(REAL * ops1, REAL * ops2);

/**
 * @brief Initialise an expression structure
 * and compute the math function expression
 * 
 * @param expr_r Expression structure to fill
 * @param expr Math expression (string)
 */
void compute_expression(EXPRESSION * expr_r, const char * expr);

/**
 * @brief Returns the result of the expression with
 * x argument as math argument
 * 
 * @param expr Expression structure
 * @param x The x argument
 * @return f(x)
 */
REAL f(EXPRESSION * expr, REAL x);

#endif // My Maths header