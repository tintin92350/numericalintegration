#include "maths.h"

int isnumeric(const char * str, const int n)
{
    for(int i = 0; i < n; i++)
        if(!isdigit(str[i]) && str[i] != '.')
            return 0;

    return 1;
}

void swap_greater(REAL * ops1, REAL * ops2)
{
    if(*ops1 > *ops2)
    {
        REAL c = *ops1;
        *ops1 = *ops2;
        *ops2 = c;
    }
}

void compute_expression(EXPRESSION * expr_r, const char * expr)
{
    expr_r->x = 0.0;
    expr_r->vars[0] = (te_variable){ "x", &expr_r->x };
    expr_r->computable = te_compile(expr, expr_r->vars, 1, &expr_r->err);
}

REAL f(EXPRESSION * expr, REAL x)
{
    expr->x = x;
    return te_eval(expr->computable);

}