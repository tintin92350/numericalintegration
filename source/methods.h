/**
 * @file methods.h
 * @author RODIC Quentin (rodic.quentin.corporation@gmail.com)
 * @brief Define all methods to compute an integral
 * @version 0.1
 * @date 2019-09-14
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#ifndef METHODS_HEADER
#define METHODS_HEADER

// Maths
#include "maths.h"

/**
 * @brief Compute the integral of an expression between
 * a and b interval
 * 
 * Use the LEFT RECTANGLE RULE formula :
 * 
 * \f[
 *  \int_{a}^bf(t)dt\approx \frac{b - a}{n} (\sum\limits_{k=0}^{k=n-1} f(a_k))
 * \f]
 * 
 * @param expr Expression used
 * @param a Interval begining
 * @param b Interval ending
 * @param n Subdivision
 * @return Integral of \f$f(x)\f$ between \f$[a, b]\f$
 */
REAL integral_by_left_rectangle_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n);

/**
 * @brief Compute the integral of an expression between
 * a and b interval
 * 
 * Use the RIGHT RECTANGLE RULE formula :
 * 
 * \f[
 *  \int_{a}^bf(t)dt\approx \frac{b - a}{n} (\sum\limits_{k=1}^{k=n} f(a_k))
 * \f]
 * 
 * @param expr Expression used
 * @param a Interval begining
 * @param b Interval ending
 * @param n Subdivision
 * @return Integral of \f$f(x)\f$ between \f$[a, b]\f$
 */
REAL integral_by_right_rectangle_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n);

/**
 * @brief Compute the integral of an expression between
 * a and b interval
 * 
 * Use the MEDIAN RECTANGLE RULE formula :
 * 
 * \f[
 *  \int_{a}^bf(t)dt\approx \frac{b - a}{n} (\sum\limits_{k=0}^{k=n-1} f(\frac{a_k + a_{k+1}}{2}))
 * \f]
 * 
 * @param expr Expression used
 * @param a Interval begining
 * @param b Interval ending
 * @param n Subdivision
 * @return Integral of \f$f(x)\f$ between \f$[a, b]\f$
 */
REAL integral_by_median_rectangle_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n);

/**
 * @brief Compute the integral of an expression between
 * a and b interval
 * 
 * Use the TRAPEZOIDAL RULE formula :
 * 
 * \f[
 *  \int_{a}^bf(t)dt\approx \frac{b - a}{2n} (f(a) + f(b) + 2\sum\limits_{k=0}^{k=n-1} f(a_k))
 * \f]
 * 
 * @param expr Expression used
 * @param a Interval begining
 * @param b Interval ending
 * @param n Subdivision
 * @return Integral of \f$f(x)\f$ between \f$[a, b]\f$
 */
REAL integral_by_trapezoidal_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n);

/**
 * @brief Compute the integral of an expression between
 * a and b interval
 * 
 * Use the SIMPSON RULE formula :
 * 
 * \f[
 *  \int_{a}^bf(t)dt\approx \frac{b - a}{6n} (f(a) + f(b) + 2\sum\limits_{k=1}^{k=n-1} f(a + \frac{(k)(b - a)}{n}) + 4\sum\limits_{k=0}^{k=n-1} f(a + \frac{(2k+1)(b-a)}{2n}))
 * \f]
 * 
 * @param expr Expression used
 * @param a Interval begining
 * @param b Interval ending
 * @param n Subdivision
 * @return Integral of \f$f(x)\f$ between \f$[a, b]\f$
 */
REAL integral_by_simpson_rule(EXPRESSION * expr, REAL a, REAL b, NATURAL n);

#endif // Methods header