# Define the binary directory relative path
BIN_PATH="bin"

# Define all files to compile
FILES_INPUT="source/main.c source/methods.c source/maths.c source/tinyexpr.c"

# Define compiler options
CC_OPT="-lm -std=c11 -m64"

# Create the binary directory if not exists
if [ ! -d "$BIN_PATH" ]; then
    mkdir $BIN_PATH
fi

# Compile the program
gcc $FILES_INPUT -o $BIN_PATH/numericalIntegration $CC_OPT

