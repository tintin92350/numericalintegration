#!/bin/bash

# Compile the program
echo "--- Compiling Numerical Interface ---"
sh compile.sh

# Create binary directory on Debian distro
mkdir /usr/bin/numericalIntegrationBin/

# Extract data
echo "--- Extracting doc to binary directory ---"
tar zxvf doc.tar.gz
mv doc /usr/bin/numericalIntegrationBin/

# Copy binary file and doc into binary directory
echo "--- Copying binary into user bin directory ---"
cp bin/numericalIntegration /usr/bin/numericalIntegrationBin/

# Install NIWI
echo "--- Installing NIWI ---"
bash install-web-interface.sh

# Settings path
ln -s /usr/bin/numericalIntegrationBin/numericalIntegration /usr/bin/numericalIntegration