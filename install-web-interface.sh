#!/bin/bash

# Function that check if a packager exists
# First argument is the package name
function check_package {
    echo "-- Checking if '$1' package is installed" >&2

    if [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -eq 0 ] ; then
        echo "error : '$1' is not installed" >&2
        echo 0
    else
        echo "success : '$1' is installed" >&2
        echo 1
    fi
}

# Check if apache is installed
apache_installed=$(check_package apache2)

# Check if php is installed
php_installed=$(check_package php)

if [ $apache_installed -eq 0 -o $php_installed -eq 0 ] ; then
    echo "You must installed the missing package to use the NIWI program"
else
    echo "- Extracting niwi web interface archives"
    tar zxvf niwi.tar.gz niwi
    sh compile.sh
    echo "- Copying NIWI to apache default directory"
    cp bin/numericalIntegration niwi/
    cp niwi /var/www/html/ -r
    echo "- Copying documentation into niwi dir"
    tar zxvf doc.tar.gz
    mv doc/ /var/www/html/niwi/
    echo "Success !"
fi
